package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
    String repoPath, object;

    public GitBlobObject(String repoPath, String s) {
        this.repoPath = repoPath;
        this.object = s;
    }

    public String getAll(){
        String path =  repoPath + "/objects/" +object.substring(0,2) + "/" + object.substring(2);
        String ris = "";
        try {
            FileInputStream fis = new FileInputStream(path);
            InflaterInputStream iis = new InflaterInputStream(fis);
            int oneByte;
            while ((oneByte = iis.read()) != -1) {
                ris += new String(new byte[] {(byte)oneByte});
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ris;

    }

    public String getType() {
        return getAll().substring(0,4);
    }

    public String getContent() {
        return getAll().substring(8);
    }
}
