package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitCommitObject {
    private String repoPath;
    private String masterCommitHash;
    private String[] master;

    public GitCommitObject(String repoPath, String masterCommitHash) {
        this.repoPath = repoPath;
        this.masterCommitHash = masterCommitHash;
    }

    public String getHash() {
       return this.masterCommitHash;
    }

    public void getAll(){
        String path =  repoPath + "/objects/" +masterCommitHash.substring(0,2) + "/" + masterCommitHash.substring(2);
        String ris = "";
        try {
            FileInputStream fis = new FileInputStream(path);
            InflaterInputStream iis = new InflaterInputStream(fis);
            int oneByte;
            while ((oneByte = iis.read()) != -1) {
                ris += new String(new byte[] {(byte)oneByte});
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.master = ris.split("\n");
    }

    public String getTreeHash() {
        if(this.master==null)
            getAll();
        String ris = master[0];
        return ris.substring(16,56);
    }

    public String getParentHash() {
        if(this.master==null)
            getAll();
        String ris = master[1];
        return ris.substring(7,47);
    }

    public String getAuthor() {
        if(this.master==null)
            getAll();
        String ris = master[2];
        String[] str = ris.split(" ");
        return str[1] + " " + str[2] + " " + str[3];
    }
}
