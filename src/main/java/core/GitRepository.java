package core;

import java.io.*;

public class GitRepository {
    private String repoPath;

    public GitRepository(String repoPath) {
        this.repoPath = repoPath;
    }

    public String getHeadRef(){
        File file = new File(repoPath+"/HEAD");
        String ris = "" ,temp = "";

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((temp = br.readLine()) != null) {
                ris += temp.substring(5);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ris;
    }

    public String getRefHash(String s) {
        File file = new File(repoPath+"/"+s);
        String ris = "" ,temp = "";

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((temp = br.readLine()) != null) {
                ris += temp;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ris;
    }
}
